# vue3+vite2+electron脚手架

#### 介绍
vue3+vite2+electron 集成方案+打包方案

#### 使用说明

1.  `electron:serve` 运行服务
2.  `electron:build` 打包程序，具体打包逻辑自行修改 electron-builder.yml 配置文件。
    配置参考[electron-builder](https://www.electron.build/)
